'''
This script is used to record time of a request being made to a Cloudfront URL or a 
Nginx site and simulate which one is better
'''
import datetime, urllib2, threading
#url_to_query = "http://VialogueLoadBalancer-1562257362.us-east-1.elb.amazonaws.com"
url_to_query = "http://pundit.tc.columbia.edu"
concurr_requests = 100
list_of_times = []
class SendConcurrentRequests(threading.Thread):
  def run(self):
 		start_date = datetime.datetime.now()
		page = urllib2.urlopen(url_to_query)
		page.read()
		end_date = datetime.datetime.now()
		list_of_times.append((end_date - start_date).microseconds/ 1000)
		
def average(list_of_times):
	print "average time for a request in milliseconds : %f" %(sum(list_of_times)/ len(list_of_times))
	

def main_program():

	program_start_time = datetime.datetime.now()	
	for itr in range(concurr_requests):
		t = SendConcurrentRequests()
		t.run()
		
	program_end_time = datetime.datetime.now()
	print "the time to complete the whole program in seconds  %f" %((program_end_time - program_start_time).seconds)
	print list_of_times
	average(list_of_times)

if __name__ == "__main__":
	main_program()
	